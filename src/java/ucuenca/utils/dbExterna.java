package ucuenca.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edisson
 */
public class dbExterna {

    private Connection conn = null;
    private String _url = "";
    private String _user = "";
    private String _pass = "";
    private String _driver = "com.mysql.jdbc.Driver";

    public dbExterna(String host, String database, String usuario, String contrasena) {
        _url = "jdbc:mysql://" + host + "/" + database + "?useSSL=false&autoCommit=false&zeroDateTimeBehavior=convertToNull";
        System.out.println(_url);
        _user = usuario;
        _pass = contrasena;
    }

    public Connection openConnection() throws Exception {
        Connection resultado = null;
        if (conn == null) {
            reconect();
        }
        if (conn != null) {
            try {
                if (conn.isClosed()) {
                    reconect();
                }
            } catch (Exception e) {
            }
        }
        resultado = conn;

        return resultado;
    }

    private void reconect() throws Exception {
        Class.forName(_driver);
        conn = DriverManager.getConnection(_url, _user, _pass);
        conn.setAutoCommit(false);
    }

    public Date getFechaDB() {
        Date resultado = null;
        try {
            Connection conn = openConnection();
            ResultSet rs = conn.createStatement().executeQuery("select current_timestamp");
            rs.next();
            resultado = rs.getTimestamp(1);
        } catch (Exception ex) {
            rollback();
            Logger.getLogger(dbExterna.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            closeConnection();
        }
        return resultado;
    }

    public void rollback() {
        if (conn != null) {
            try {
                conn.rollback();
            } catch (Exception e) {
            }
        }
    }

    public void closeConnection() {
        if (conn != null) {
            try {
                conn.close();
                conn = null;
            } catch (Exception e) {
            }
        }
    }

    public List<Object[]> query(Connection connection, String sql) {
        return query(connection, sql, false);
    }

    public List<Object[]> query(Connection connection, String sql, boolean printOut) {
        List<Object[]> resultado = new ArrayList<>();
        try {
            Connection conn;
            if (connection == null) {
                conn = openConnection();
            } else {
                conn = connection;
            }
            Statement st = conn.createStatement();
            st.closeOnCompletion();
            ResultSet rs = st.executeQuery(sql);
            int cols = rs.getMetaData().getColumnCount();
            System.out.print(printOut ? "|" : "");
            for (int i = 0; i < cols; i++) {
                String name = rs.getMetaData().getColumnName(i + 1);
                System.out.print(printOut ? name + "\t|" : "");
            }
            List<Object[]> aux = new ArrayList<>();
            while (rs.next()) {
                System.out.print(printOut ? "\n|" : "");
                Object[] fila = new Object[cols];
                for (int i = 0; i < cols; i++) {
                    fila[i] = rs.getObject(i + 1);
                    System.out.print(printOut ? fila[i] + "\t|" : "");
                }
                aux.add(fila);
            }
            rs.close();
            System.out.print(printOut ? "\n\n" + aux.size() + " Filas " : "");
            resultado.addAll(aux);
            if (connection == null) {
                conn.commit();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            if (connection == null) {
                rollback();
            }
        } finally {
            if (connection == null) {
                closeConnection();
            }
        }
        return resultado;
    }

    public InfoTabla resultSet(Connection connection, String sql) {
        InfoTabla resultado = null;
        try {
            Connection conn;
            if (connection == null) {
                conn = openConnection();
            } else {
                conn = connection;
            }
            ResultSet rs = conn.createStatement().executeQuery(sql);
            int cols = rs.getMetaData().getColumnCount();
            List<String> colus = new ArrayList<String>();
            for (int i = 0; i < cols; i++) {
                String name = rs.getMetaData().getColumnName(i + 1);
                colus.add(name);
            }
            List<Object[]> aux = new ArrayList<>();
            while (rs.next()) {
                Object[] fila = new Object[cols];
                for (int i = 0; i < cols; i++) {
                    fila[i] = rs.getObject(i + 1);
                }
                aux.add(fila);
            }
            resultado = new InfoTabla(colus, aux);
            if (connection == null) {
                conn.commit();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            if (connection == null) {
                rollback();
            }
        } finally {
            if (connection == null) {
                closeConnection();
            }
        }
        return resultado;
    }

    public int update(Connection connection, String sql) throws Exception {
        return update(connection, sql, false);
    }

    public int update(Connection connection, String sql, boolean show) throws Exception {
        Exception error = null;
        int resultado = -1;
        try {
            Connection conn;
            if (connection == null) {
                conn = openConnection();
            } else {
                conn = connection;
            }
            if (show) {
                System.out.println(sql);
            }
            int aux = conn.createStatement().executeUpdate(sql);
            if (connection == null) {
                conn.commit();
            }
            resultado = aux;
        } catch (Exception e) {
            System.err.println(sql);
            if (connection == null) {
                rollback();
            }
            error = e;
        } finally {
            if (connection == null) {
                closeConnection();
            }
        }
        if (error != null) {
            throw error;
        }
        return resultado;
    }

    public Date getFechaDB(Connection conn) throws Exception {
        Date fecha = null;
        List<Object[]> aux = query(conn, "select now()");
        if (aux != null) {
            if (!aux.isEmpty()) {
                fecha = (Date) aux.get(0)[0];
            }
        }
        return fecha;
    }

    public class InfoTabla {

        private List<String> _columnas;
        private List<Object[]> _data;

        public InfoTabla(List<String> _columnas, List<Object[]> _data) {
            this._columnas = _columnas;
            this._data = _data;
        }

        public List<String> getColumnas() {
            return _columnas;
        }

        public void setColumnas(List<String> _columnas) {
            this._columnas = _columnas;
        }

        public List<Object[]> getData() {
            return _data;
        }

        public void setData(List<Object[]> _data) {
            this._data = _data;
        }

    }
}
