/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucuenca.model;

import java.util.ArrayList;
import java.util.List;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.sparql.vocabulary.FOAF;
import ucuenca.beans.CtrApp;
import ucuenca.beans.CtrRdf;

/**
 *
 * @author edisson
 */
public class Persona {

    private Individual _ind;
    private List<Statement> _sts = new ArrayList<>();
    private List<Statement> _tipos = new ArrayList<>();
    private List<Individual> _regIess = new ArrayList<>();
    private List<Individual> _regSenescyt = new ArrayList<>();

    public Persona(CtrApp bean, CtrRdf rdf, Individual _ind) {
        this._ind = _ind;
        analiza(bean, rdf);

    }

    public String getIRI() {
        return _ind.getURI();
    }

    public void analiza(CtrApp bean, CtrRdf rdf) {
        List<Statement> sts = _ind.listProperties().toList();
        OntProperty pIess = bean.getModel().getOntProperty(bean.NS + "trabajoEn");
        OntProperty pSenes = bean.getModel().getOntProperty(bean.NS + "estudioEn");
//        System.out.println("pro = " + pro);
        for (Statement st : sts) {
            if (st.getPredicate().getURI() != null) {
                if (st.getPredicate().getURI().equals(pIess.getURI())) {
                    Individual ind = bean.getModel().getIndividual(st.getObject().asResource().toString());
                    _regIess.add(ind);
                    _sts.add(st);
                } else if (st.getPredicate().getURI().equals(pSenes.getURI())) {
                    Individual ind = bean.getModel().getIndividual(st.getObject().asResource().toString());
                    _regSenescyt.add(ind);
                    _sts.add(st);
                } else if (st.getPredicate().getURI().equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
                    if (st.getObject().isURIResource()) {
                        if (!st.getObject().asResource().getURI().equals("http://www.w3.org/2002/07/owl#Thing")
                                && !st.getObject().asResource().getURI().equals("http://www.w3.org/2000/01/rdf-schema#Resource")) {
                            _tipos.add(st);
                        }
                    }
                } else {
                    if (st.getPredicate().getURI().equals(FOAF.name.getURI())) {
//                        System.out.println("*********************");
//                        System.out.println(_ind.getURI());
//                        System.out.println(st.getObject().asLiteral().getString());
                        rdf.addLabel(_ind.getURI(), st.getObject().asLiteral().getString());
                    }
                    if (!st.getPredicate().getURI().equals("http://www.w3.org/2002/07/owl#differentFrom") && !st.getPredicate().getURI().equals("http://www.w3.org/2002/07/owl#sameAs")) {
                        _sts.add(st);
                    }
                }
            }
        }
    }

    public Individual getInd() {
        return _ind;
    }

    public void setInd(Individual _ind) {
        this._ind = _ind;
    }

    public List<Statement> getSts() {
        return _sts;
    }

    public void setSts(List<Statement> _sts) {
        this._sts = _sts;
    }

    public List<Individual> getRegIess() {
        return _regIess;
    }

    public void setRegIess(List<Individual> _regIess) {
        this._regIess = _regIess;
    }

    public List<Individual> getRegSenescyt() {
        return _regSenescyt;
    }

    public void setRegSenescyt(List<Individual> _regSenescyt) {
        this._regSenescyt = _regSenescyt;
    }

    public List<Statement> getTipos() {
        return _tipos;
    }

    public void setTipos(List<Statement> _tipos) {
        this._tipos = _tipos;
    }

}
