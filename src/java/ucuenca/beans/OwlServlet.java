/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucuenca.beans;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.jena.rdf.model.Statement;

/**
 *
 * @author edisson
 */
public class OwlServlet extends HttpServlet {

    @Inject
    private CtrApp _bean;

    @Override
    public void service(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException, ServletException {
        ServletOutputStream ouputStream = null;
        String path = request.getPathInfo();
        try {
            ouputStream = response.getOutputStream();
            if (_bean.getModel() != null) {
                response.setContentType("text/xml");
                if (path != null || path.equals("/")) {
//                    List<Statement> sts = _bean.getModel().listStatements().toList();
//                    for (Statement st : sts) {
//                        ouputStream.write((st.toString() + "\n").getBytes());
//                    }
                    _bean.getModel().write(ouputStream);
                }
//                response.setContentLength();
//                ouputStream.write(data);
            } else {
                ouputStream.write("No se encuentra inicializado el modelo".getBytes());
            }
        } catch (Exception e) {
            throw new ServletException(e);
        } finally {
            ouputStream.close();
        }
    }

}
