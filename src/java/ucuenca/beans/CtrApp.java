/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucuenca.beans;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.OWL2;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ucuenca.utils.FacesUtils;
import ucuenca.utils.dbExterna;

/**
 *
 * @author edisson
 */
@Named(value = "ctrApp")
@ApplicationScoped
public class CtrApp {

    private OntModel _model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_RULE_INF);
    public static final String NS = "http://sovi.ddns.net/ucuenca/tfinal/";
//    private final String NS = "http://www.semanticweb.org/edisson/ontologies/2019/1/untitled-ontology-13";
    private final String REL = "http://purl.org/vocab/relationship/";
    private final String OWL_FILE = "/home/edisson/pyme/owl/tfinal.owl";
    private final String RDF_FILE = "/home/edisson/pyme/owl/FINAL.foaf";
    private final String XLSX_FILE = "/home/edisson/pyme/owl/senescyt.xlsx";
    private boolean _procesando = false;

    private OntProperty pCedula;
    private OntProperty pEstadoCivil;
    private Property pChildOf;
    private Property pSpouseOf;

    public CtrApp() {
    }

    public void procesar() {
        if (_procesando) {
            FacesUtils.msgWarning("Mensaje", "Se encuentra otro proceso activo en este momento...!!");
            return;
        }
        _procesando = true;
        try {
            _model = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_RULE_INF);
            _model.read(OWL_FILE, "RDF/XML");
            InputStream IS_REL = new URL("http://vocab.org/relationship/rel-vocab-20100607.rdf").openStream();
            _model.read(IS_REL, "RDF/XML");

            pCedula = _model.getOntProperty(NS + "cedula");
            pEstadoCivil = _model.getOntProperty(NS + "estadoCivil");
            pChildOf = _model.getProperty(REL + "childOf");
            pSpouseOf = _model.getOntProperty(REL + "spouseOf");
            leerBase();
            leerRdf();
            leerExcel();
            FacesUtils.msgInfo("Mensaje", "Proceso terminado");
        } catch (Exception e) {
            FacesUtils.msgError("Error", e.getMessage());
            e.printStackTrace();

        }
        _procesando = false;
    }

    private void leerRdf() throws Exception {
//        InputStream IS = new URL("http://facturas.recordcalza.com/download/iess.rdf").openStream();
//        _model.read(IS, "TURTLE");
//        IS.close();
        _model.read(RDF_FILE, "TURTLE");
    }

    private void leerExcel() throws Exception {
//        InputStream IS = new URL("http://facturas.recordcalza.com/download/senescyt.xlsx").openStream();
//        XSSFWorkbook myWorkBook = new XSSFWorkbook(IS);
        FileInputStream fis = new FileInputStream(XLSX_FILE);
        XSSFWorkbook myWorkBook = new XSSFWorkbook(fis);
        // Finds the workbook instance for XLSX file
        XSSFSheet mySheet = myWorkBook.getSheetAt(0);
        Iterator<Row> rowIterator = mySheet.iterator();
        // Traversing over each row of XLSX file
        int cont = 0;
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            cont++;
            if (cont > 1) {
                Cell cA = row.getCell(0);
                String reg = cA == null ? null : cA.getStringCellValue();
                Cell cB = row.getCell(1);
                String cedula = cB == null ? null : cB.getStringCellValue();
                Cell cC = row.getCell(2);
                String universidad = cC == null ? null : cC.getStringCellValue();
                Cell cD = row.getCell(3);
                String tipo = cD == null ? null : cD.getStringCellValue();
                Cell cE = row.getCell(4);
                String carrera = cE == null ? null : cE.getStringCellValue();
                Cell cF = row.getCell(5);
                Integer aIngreso = cF == null ? null : new BigDecimal(cF.getNumericCellValue()).intValue();
                Cell cG = row.getCell(6);
                Integer aEgreso = cG == null ? null : new BigDecimal(cG.getNumericCellValue()).intValue();
                Cell cH = row.getCell(7);
                Integer aGraduacion = cH == null ? null : new BigDecimal(cH.getNumericCellValue()).intValue();
                Cell cI = row.getCell(8);
                String estado = cI == null ? null : cI.getStringCellValue();
                Resource rA = _model.getResource(NS + "RegistroAcademico");
                Individual indv = _model.createIndividual(NS + "" + reg, rA);
                indv.setPropertyValue(_model.getProperty(NS + "numReg"), _model.createLiteral(reg));
                if (cedula != null) {
                    indv.addProperty(_model.getProperty(NS + "estudiante"), _model.getResource(NS + "" + cedula));
                }
                if (universidad != null) {
                    indv.setPropertyValue(_model.getProperty(NS + "universidad"), _model.createLiteral(universidad));
                }
                if (tipo != null) {
                    indv.setPropertyValue(_model.getProperty(NS + "tipoCarrera"), _model.createLiteral(tipo));
                }
                if (carrera != null) {
                    indv.setPropertyValue(_model.getProperty(NS + "nombreCarrera"), _model.createLiteral(carrera));
                }
                if (aIngreso != null) {
                    indv.setPropertyValue(_model.getProperty(NS + "anioIngreso"), _model.createTypedLiteral(aIngreso));
                }
                if (aEgreso != null) {
                    indv.setPropertyValue(_model.getProperty(NS + "anioEgreso"), _model.createTypedLiteral(aEgreso));
                }
                if (aGraduacion != null) {
                    indv.setPropertyValue(_model.getProperty(NS + "anioGraduacion"), _model.createTypedLiteral(aGraduacion));
                }
                if (estado != null) {
                    indv.setPropertyValue(_model.getProperty(NS + "estado"), _model.createTypedLiteral(estado));
                }
            }
        }
        myWorkBook.close();
//        IS.close();
    }

    public void leerBase() {
        dbExterna db = new dbExterna("192.168.100.199", "rs", "ucuenca", "ucuenca");
        List<Object[]> ciudadanos = db.query(null, "select * From persona");
        for (Object[] ciud : ciudadanos) {
            String cedula = ciud[0].toString();
            String nombre = ciud[1].toString();
            Date fecha = (Date) ciud[2];
            String sexo = ciud[3].toString();
            String estadoCivil = ciud[4].toString();
            String padre = ciud[5] == null ? null : ciud[5].toString();
            String madre = ciud[6] == null ? null : ciud[6].toString();;
            String conyugue = ciud[7] == null ? null : ciud[7].toString();;
            Individual indv = _model.createIndividual(NS + "" + cedula, FOAF.Person);
            indv.setPropertyValue(pCedula, _model.createLiteral(cedula));
            indv.setPropertyValue(FOAF.gender, _model.createLiteral(sexo.equals("M") ? "male" : "female"));
            indv.setPropertyValue(FOAF.name, _model.createLiteral(nombre));
            indv.setPropertyValue(pEstadoCivil, _model.createLiteral(
                    estadoCivil.equals("C") ? "Casado/a"
                    : estadoCivil.equals("S") ? "Soltero/a"
                    : estadoCivil.equals("D") ? "Divorciado/a"
                    : estadoCivil.equals("V") ? "Viudo/a"
                    : estadoCivil
            ));
            Calendar cal = Calendar.getInstance();
            cal.setTime(fecha);
            indv.setPropertyValue(FOAF.birthday, _model.createTypedLiteral(cal));
            indv.setPropertyValue(FOAF.age, _model.createTypedLiteral(getAge(fecha)));
            if (padre != null) {
//                indv.addProperty(pHijoDe, _model.getResource("ci:" + padre));
                indv.addProperty(pChildOf, _model.getResource(NS + "" + padre));
            }
            if (madre != null) {
//                indv.addProperty(pHijoDe, _model.getResource("ci:" + madre));
                indv.addProperty(pChildOf, _model.getResource(NS + "" + madre));
            }
            if (conyugue != null) {
                indv.addProperty(pSpouseOf, _model.getResource(NS + "" + conyugue));
            }
        }
//        _model.write(System.out);
    }

    public static int getAge(Date dateOfBirth) {
        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();
        birthDate.setTime(dateOfBirth);
        if (birthDate.after(today)) {
            throw new IllegalArgumentException("You don't exist yet");
        }
        int todayYear = today.get(Calendar.YEAR);
        int birthDateYear = birthDate.get(Calendar.YEAR);
        int todayDayOfYear = today.get(Calendar.DAY_OF_YEAR);
        int birthDateDayOfYear = birthDate.get(Calendar.DAY_OF_YEAR);
        int todayMonth = today.get(Calendar.MONTH);
        int birthDateMonth = birthDate.get(Calendar.MONTH);
        int todayDayOfMonth = today.get(Calendar.DAY_OF_MONTH);
        int birthDateDayOfMonth = birthDate.get(Calendar.DAY_OF_MONTH);
        int age = todayYear - birthDateYear;

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ((birthDateDayOfYear - todayDayOfYear > 3) || (birthDateMonth > todayMonth)) {
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        } else if ((birthDateMonth == todayMonth) && (birthDateDayOfMonth > todayDayOfMonth)) {
            age--;
        }
        return age;
    }

    public void analizaModel() {
//        System.out.pri_model.listIndividuals().toList()ntln("*****");
        for (Individual indv : _model.listIndividuals().toList()) {
            indv.addRDFType(OWL2.NamedIndividual);
            if (indv.getURI() != null) {
                System.out.println("********************************");
                for (Resource tipo : indv.listRDFTypes(true).toList()) {
                    if (tipo.getURI() != null) {
                        if (!tipo.getURI().equals("http://www.w3.org/2002/07/owl#NamedIndividual") && !tipo.getURI().equals("http://www.w3.org/2002/07/owl#ObjectProperty")) {
                            System.out.println(indv.getURI() + " - Inferido: " + tipo.getURI());
                        }
                    }
                }
                List<Statement> sts = indv.listProperties().toList();
                for (Statement st : sts) {
                    Property prop = st.getPredicate();
                    System.out.print(prop.getLocalName());
                    System.out.print("\t");
                    System.out.println(st.getObject().toString());

                }

            }
        }
        ;
//        Individual ind = _model.getIndividual(_model.getNsPrefixURI("") + "0105247555");
    }

    public void analizaMode2l() {
        List<Statement> sts = _model.listStatements().toList();
        for (Statement st : sts) {
            System.out.println(st.toString());
        }
    }

    //------------------------------------------------------------------------
    //----GETTERS Y SETTERS---------------------------------------------------
    //------------------------------------------------------------------------
    public OntModel getModel() {
        return _model;
    }

    public void setModel(OntModel _model) {
        this._model = _model;
    }

}
