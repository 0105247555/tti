/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucuenca.beans;

import java.io.IOException;
import java.util.Enumeration;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author edisson
 */
public class RdfsServlet extends HttpServlet {

    @Inject
    private CtrApp _bean;

    @Override
    public void service(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException, ServletException {
        ServletOutputStream ouputStream = null;
        String path = request.getPathInfo();
        System.out.println("*****");
        System.out.println(path);
        System.out.println("*****");
        try {
            ouputStream = response.getOutputStream();
            if (_bean.getModel() != null) {
                if (path == null || path.equals("/")) {
                    response.setContentType("text/xml");
//                    List<Statement> sts = _bean.getModel().listStatements().toList();
//                    for (Statement st : sts) {
//                        ouputStream.write((st.toString() + "\n").getBytes());
//                    }
                    _bean.getModel().write(ouputStream);
//                    _bean.getModel().write(ouputStream, "turtle");
                } else {
                    response.setContentType("text/xhtml");
                    ouputStream.write(path.getBytes());
                }
//                response.setContentLength();
//                ouputStream.write(data);
            } else {
                ouputStream.write("No se encuentra inicializado el modelo".getBytes());
            }
        } catch (Exception e) {
            throw new ServletException(e);
        } finally {
            ouputStream.close();
        }
    }

}
