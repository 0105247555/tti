/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ucuenca.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import org.apache.jena.ontology.Individual;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import ucuenca.model.Persona;

/**
 *
 * @author edisson
 */
@Named(value = "ctrRdf")
@ViewScoped
public class CtrRdf implements Serializable {

    @Inject
    private CtrApp _bean;
    private List<Persona> _individuos = new ArrayList<>();
    private Map<String, String> _labels = new HashMap<String, String>();

    public CtrRdf() {
        _labels.put("http://purl.org/vocab/relationship/childOf", "Hijo De");
        _labels.put("http://xmlns.com/foaf/0.1/birthday", "Fecha de Nacimiento");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/estadoCivil", "Estado Civil");
        _labels.put("http://xmlns.com/foaf/0.1/name", "Nombre");
        _labels.put("http://xmlns.com/foaf/0.1/gender", "Genero");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/cedula", "Cádula");
        _labels.put("http://xmlns.com/foaf/0.1/knows", "Conoce A");
        _labels.put("http://purl.org/vocab/relationship/knowsOf", "Conocido Por");
        _labels.put("http://purl.org/vocab/relationship/spouseOf", "Esposo/a De");
        _labels.put("http://purl.org/vocab/relationship/parentOf", "Padre De");
        _labels.put("http://xmlns.com/foaf/0.1/Person", "Persona");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/Mujer", "Mujer");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/Hombre", "Hombre");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/Padre", "Padre");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/Ecuatoriano", "Ecuatoriano/a");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/estudioEn", "Registro Academico");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/trabajoEn", "Registro Laboral");
        _labels.put("http://xmlns.com/foaf/0.1/age", "Edad");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/RegistroAcademico", "Registro Académico");
        _labels.put("http://sovi.ddns.net/ucuenca/tfinal/RegistroLaboral", "Registro Laboral");
    }

    public void addLabel(String prop, String label) {
        _labels.put(prop, label);
    }

    public String label(String uri) {
        String res = _labels.get(uri);
        return res == null ? uri : res;
    }

    @PostConstruct
    public void analize() {
        List<Individual> inds = _bean.getModel().listIndividuals().toList();
        for (Individual ind : inds) {
            try {
                Persona p = new Persona(_bean, this, ind);
//                if (ind.getURI().contains("0105247555") || ind.getURI().contains("0103978441")) {
                _individuos.add(p);
//                }
            } catch (Exception e) {
            }
        }

    }

    public Statement fff(Individual ind, String prop) {
        Statement st = ind.getProperty(_bean.getModel().getProperty(prop));
        return st;
    }

    public String dato(RDFNode nod) {
        String res = "";
        if (nod != null) {
            if (nod.isLiteral()) {
                res = nod.asLiteral().getString();
            } else if (nod.isURIResource()) {
                res = label(nod.asResource().getURI());
                res = res == null ? nod.asResource().getURI() : res;
            }
        }
        return res;
    }

    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    public List<Persona> getIndividuos() {
        return _individuos;
    }

    public void setIndividuos(List<Persona> _individuos) {
        this._individuos = _individuos;
    }

}
